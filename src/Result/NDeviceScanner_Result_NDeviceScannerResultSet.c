#include "../../include/NDeviceScanner.h"

// ------------------------------------------------------
// struct NDeviceScanner::Result::NDeviceScannerResultSet
// ------------------------------------------------------

/**
 * Build a scan result set
 *
 * @param ip
 * 		The IPv4
 * @param cidr
 * 		The cidr
 *
 * @return the scan result
 */
__ALLOC NDeviceScannerResultSet *NDeviceScanner_Result_NDeviceScannerResultSet_Build( const char *ip,
	NU32 cidr )
{
	// Output
	__OUTPUT NDeviceScannerResultSet *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NDeviceScannerResultSet ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Create output list
	if( !( out->m_list = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NDeviceScanner_Result_NDeviceScannerResult_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Duplicate ip
	if( !( out->m_ip = NLib_Chaine_Dupliquer( ip ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NLib_Memoire_NListe_Detruire( &out->m_list );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_cidr = cidr;

	// OK
	return out;
}

/**
 * Destroy scan result set
 *
 * @param this
 * 		This instance
 */
void NDeviceScanner_Result_NDeviceScannerResultSet_Destroy( NDeviceScannerResultSet **this )
{
	// Destroy list
	NLib_Memoire_NListe_Detruire( &(*this)->m_list );

	// Free
	NFREE( (*this)->m_ip );
	NFREE( (*this) );
}

/**
 * Get result count
 *
 * @param this
 * 		This instance
 *
 * @return the result count
 */
NU32 NDeviceScanner_Result_NDeviceScannerResultSet_GetCount( const NDeviceScannerResultSet *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_list );
}

/**
 * Get result through index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The index
 *
 * @return the result
 */
const NDeviceScannerResult *NDeviceScanner_Result_NDeviceScannerResultSet_Get( const NDeviceScannerResultSet *this,
	NU32 index )
{
	return (NDeviceScannerResult*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
		index );
}

/**
 * Add result
 *
 * @param this
 * 		This instance
 * @param result
 * 		The result to add
 *
 * @return if operation succeedeed
 */
NBOOL NDeviceScanner_Result_NDeviceScannerResultSet_Add( NDeviceScannerResultSet *this,
	__WILLBEOWNED NDeviceScannerResult *result )
{
	// Output
	__OUTPUT NBOOL output;

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_list );

	// Add
	output = NLib_Memoire_NListe_Ajouter( this->m_list,
		result );

	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_list );

	// OK?
	return output;
}

