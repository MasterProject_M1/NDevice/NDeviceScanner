#include "../../include/NDeviceScanner.h"

// ---------------------------------------------------
// struct NDeviceScanner::Result::NDeviceScannerResult
// ---------------------------------------------------

/**
 * Build a device
 *
 * @param type
 * 		The device type
 * @param ip
 * 		The ip
 * @param uuid
 * 		The device uuid
 * @param name
 * 		The device name
 * @param requestingIP
 * 		The requesting IP
 *
 * @return the device scanner result instance
 */
__ALLOC NDeviceScannerResult *NDeviceScanner_Result_NDeviceScannerResult_Build( NDeviceType type,
	const char *ip,
	const char *uuid,
	const char *name,
	const char *requestingIP )
{
	// Output
	__OUTPUT NDeviceScannerResult *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NDeviceScannerResult ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate data
	if( !( out->m_ip = NLib_Chaine_Dupliquer( ip ) )
		|| !( out->m_uuid = NLib_Chaine_Dupliquer( uuid ) )
		|| !( out->m_name = NLib_Chaine_Dupliquer( name ) )
		|| ( requestingIP != NULL && !( out->m_requestingIP = NLib_Chaine_Dupliquer( requestingIP ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Free
		NFREE( out->m_name );
		NFREE( out->m_uuid );
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_type = type;

	// OK
	return out;
}

/**
 * Destroy a device
 *
 * @param this
 * 		This instance
 */
void NDeviceScanner_Result_NDeviceScannerResult_Destroy( NDeviceScannerResult **this )
{
	NFREE( (*this)->m_requestingIP );
	NFREE( (*this)->m_name );
	NFREE( (*this)->m_uuid );
	NFREE( (*this)->m_ip );
	NFREE( (*this) );
}

/**
 * Get ip
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NDeviceScanner_Result_NDeviceScannerResult_GetIP( const NDeviceScannerResult *this )
{
	return this->m_ip;
}

/**
 * Get type
 *
 * @param this
 * 		This instance
 *
 * @return the type
 */
NDeviceType NDeviceScanner_Result_NDeviceScannerResult_GetType( const NDeviceScannerResult *this )
{
	return this->m_type;
}

/**
 * Get UUID
 *
 * @param this
 * 		This instance
 *
 * @return the uuid
 */
const char *NDeviceScanner_Result_NDeviceScannerResult_GetUUID( const NDeviceScannerResult *this )
{
	return this->m_uuid;
}

/**
 * Get name
 *
 * @param this
 * 		This instance
 *
 * @return the name
 */
const char *NDeviceScanner_Result_NDeviceScannerResult_GetName( const NDeviceScannerResult *this )
{
	return this->m_name;
}

/**
 * Get requesting ip
 *
 * @param this
 * 		This instance
 *
 * @return the requesting ip
 */
const char *NDeviceScanner_Result_NDeviceScannerResult_GetRequestingIP( const NDeviceScannerResult *this )
{
	return this->m_requestingIP == NULL ?
		""
		: this->m_requestingIP;
}
