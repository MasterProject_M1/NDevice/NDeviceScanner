#include "../include/NDeviceScanner.h"

// ------------------------
// namespace NDeviceScanner
// ------------------------

#ifdef NDEVICE_SCANNER_TEST_PROJECT
/**
 * Callback status
 *
 * @param message
 * 		The message
 */
__CALLBACK void NDeviceScanner_CallbackMessageStatus( const char *message )
{
	// Display
	puts( message );
}

/**
 * Test entry point
 *
 * @param argc
 * 		The argument count
 * @param argv
 * 		The argument vector
 *
 * @return EXIT_SUCCESS if executed correctly
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Result set
	NDeviceScannerResultSet *set;

	// Result
	const NDeviceScannerResult *result;

	// Iterator
	NU32 i = 0;

	// Scanner
	NDeviceScanner *scanner;

	// Init NLib
	NLib_Initialiser( NULL );

	// Look for known devices types on 192.168.1.0/24 network
	if( !( scanner = NDeviceScanner_NDeviceScanner_Build( 50,
		"192.168.0.0",
		24,
		// Connection timeout
		700,
		// Request answer timeout
		2000,
		// Status update text callback
		NDeviceScanner_CallbackMessageStatus ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit NLib
		NLib_Detruire( );

		// Quit
		return EXIT_FAILURE;
	}

	// Scan
	set = NDeviceScanner_NDeviceScanner_Scan( scanner );

	// Destroy scanner
	NDeviceScanner_NDeviceScanner_Destroy( &scanner );

	// Display results
	for( ; i < NDeviceScanner_Result_NDeviceScannerResultSet_GetCount( set ); i++ )
	{
		// Get result
		result = NDeviceScanner_Result_NDeviceScannerResultSet_Get( set,
			i );

		// Display result
		printf( "%s: Type = %s, UUID=%s, Name=%s\n",
			NDeviceScanner_Result_NDeviceScannerResult_GetIP( result ),
			NDeviceCommon_Type_NDeviceType_GetName( NDeviceScanner_Result_NDeviceScannerResult_GetType( result ) ),
			NDeviceScanner_Result_NDeviceScannerResult_GetUUID( result ),
			NDeviceScanner_Result_NDeviceScannerResult_GetName( result ) );
	}

	// Destroy result set
	NDeviceScanner_Result_NDeviceScannerResultSet_Destroy( &set );

	// Detruire NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

#endif // NDEVICE_SCANNER_TEST_PROJECT


