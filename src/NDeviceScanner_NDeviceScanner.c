#include "../include/NDeviceScanner.h"

// -------------------------------------
// struct NDeviceScanner::NDeviceScanner
// -------------------------------------

/**
 * Callback
 *
 * @param packet
 * 		The received packet
 * @param client
 * 		The client who received the packet
 *
 * @return if operation succeedeed
 */
__PRIVATE __CALLBACK NBOOL NDeviceScanner_NDeviceScanner_CallbackReceive( const NPacket *packet,
	const NClient *client )
{
	// HTTP Client
	NClientHTTP *HTTPClient;

	// Raw uuid
	const char *rawUUID = NULL;

	// Is UUID to be processed?
	NBOOL isUUIDMustBeProcessed = NTRUE;

	// Raw integer uuid
	NU64 rawConvertedUUID = 0;

	// Current state
	NDeviceScannerCurrentState *currentState;

	// Protected data
	char *protectedData;

	// Answer
	NReponseHTTP *answer;

	// Data
	char *data;

	// Parser output list
	NParserOutputList *parserResult;

	// Parser output
	const NParserOutput *parserEntry;

	// Get instance
	if( !( HTTPClient = NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client ) )
		|| !( currentState = NHTTP_Client_NClientHTTP_ObtenirDataUtilisateur( HTTPClient ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Allocate protected data
	if( !( protectedData = calloc( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) + 1,
		sizeof( char ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NFALSE;
	}

	// Copy
	memcpy( protectedData,
		NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) );

	// Parse answer
	if( !( answer = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire2( protectedData ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free
		NFREE( protectedData );

		// Quit
		return NFALSE;
	}

	// Free
	NFREE( protectedData );

	// Get data
	if( !( data = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCopieData( answer ) ) )
	{
		// Generate empty data
		if( !( data = NLib_Chaine_Dupliquer( "{}" ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_COPY );

			// Free
			NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

			// Quit
			return NFALSE;
		}
	}

	// Parse content
	switch( currentState->m_currentDeviceType )
	{
		case NDEVICE_TYPE_MFI_MPOWER:
			parserResult = NParser_Output_NParserOutputList_Build( );
			break;

		default:
			parserResult = NJson_Engine_Parser_Parse( data,
				(NU32)strlen( data ) );
			break;
	}

	// Check parsing
	if( !parserResult )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Destroy response
		NFREE( data );
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

		// Quit
		return NFALSE;
	}

	// Save response code
	currentState->m_responseCode = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCode( answer );

	// Free
	NFREE( data );

	// Ghost device?
	if( NDeviceCommon_Type_NDeviceType_IsGhostDevice( currentState->m_currentDeviceType ) )
	{
		// Get type
		if( !( parserEntry = NParser_Output_NParserOutputList_GetFirstOutput( parserResult,
			NDEVICE_COMMON_TYPE_GHOST_DEVICE_TYPE,
			NTRUE,
			NTRUE ) ) )
		{
			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserResult );

			// Destroy HTTP response
			NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

			// Quit
			return NFALSE;
		}

		// Check type
		if( NParser_Output_NParserOutput_GetType( parserEntry ) != NPARSER_OUTPUT_TYPE_STRING
			|| !NLib_Chaine_Comparer( NParser_Output_NParserOutput_GetValue( parserEntry ),
			NDeviceCommon_Type_NDeviceType_GetName( currentState->m_currentDeviceType ),
			NTRUE,
			0 ) )
		{
			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserResult );

			// Destroy HTTP response
			NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

			// Quit
			return NFALSE;
		}

		// Get UUID
		if( !( parserEntry = NParser_Output_NParserOutputList_GetFirstOutput( parserResult,
			NDEVICE_COMMON_TYPE_GHOST_UUID_KEY,
			NTRUE,
			NTRUE ) ) )
		{
			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserResult );

			// Destroy HTTP response
			NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

			// Quit
			return NFALSE;
		}


		// Read uuid
		if( NParser_Output_NParserOutput_GetType( parserEntry ) != NPARSER_OUTPUT_TYPE_STRING
			|| !( rawUUID = NParser_Output_NParserOutput_GetValue( parserEntry ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserResult );

			// Destroy HTTP response
			NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

			// Quit
			return NFALSE;
		}

		// Get name
		if( !( parserEntry = NParser_Output_NParserOutputList_GetFirstOutput( parserResult,
			NDEVICE_COMMON_TYPE_GHOST_DEVICE_NAME,
			NTRUE,
			NTRUE ) ) )
		{
			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserResult );

			// Destroy HTTP response
			NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

			// Quit
			return NFALSE;
		}

		// Check type
		if( NParser_Output_NParserOutput_GetType( parserEntry ) != NPARSER_OUTPUT_TYPE_STRING )
		{
			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserResult );

			// Destroy HTTP response
			NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

			// Quit
			return NFALSE;
		}

		// Save
		currentState->m_deviceName = NParser_Output_NParserOutput_GetValueCopy( parserEntry );

		// Save requesting ip
		if( !( parserEntry = NParser_Output_NParserOutputList_GetFirstOutput( parserResult,
			NDEVICE_COMMON_TYPE_GHOST_YOUR_IP_KEY,
			NTRUE,
			NTRUE ) )
			|| NParser_Output_NParserOutput_GetType( parserEntry ) != NPARSER_OUTPUT_TYPE_STRING )
		{
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserResult );

			// Destroy HTTP response
			NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

			// Quit
			return NFALSE;
		}

		// Duplicate
		currentState->m_requestingIP = NParser_Output_NParserOutput_GetValueCopy( parserEntry );
	}
	// Not a ghost device
	else
	{
		switch( currentState->m_currentDeviceType )
		{
			case NDEVICE_TYPE_HUE:
				// Look for name
				if( !( parserEntry = NParser_Output_NParserOutputList_GetFirstOutput( parserResult,
					NDEVICE_COMMON_TYPE_HUE_NAME_KEY,
					NTRUE,
					NTRUE ) ) )
				{
					// Destroy
					NParser_Output_NParserOutputList_Destroy( &parserResult );

					// Destroy HTTP response
					NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

					// Quit
					return NFALSE;
				}

				// Check name
				if( NParser_Output_NParserOutput_GetType( parserEntry ) != NPARSER_OUTPUT_TYPE_STRING
					|| !NLib_Chaine_Comparer( NParser_Output_NParserOutput_GetValue( parserEntry ),
					NDEVICE_COMMON_TYPE_CORRECT_HUE_NAME,
					NTRUE,
					0 ) )
				{
					// Destroy
					NParser_Output_NParserOutputList_Destroy( &parserResult );

					// Destroy HTTP response
					NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

					// Quit
					return NFALSE;
				}

				// Look for uuid
				if( !( parserEntry = NParser_Output_NParserOutputList_GetFirstOutput( parserResult,
					NDEVICE_COMMON_TYPE_HUE_UUID_KEY,
					NTRUE,
					NTRUE ) ) )
				{
					// Destroy
					NParser_Output_NParserOutputList_Destroy( &parserResult );

					// Destroy HTTP response
					NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

					// Quit
					return NFALSE;
				}

				// Read uuid
				if( NParser_Output_NParserOutput_GetType( parserEntry ) != NPARSER_OUTPUT_TYPE_STRING
					|| !( rawUUID = NParser_Output_NParserOutput_GetValue( parserEntry ) ) )
				{
					// Notify
					NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

					// Destroy
					NParser_Output_NParserOutputList_Destroy( &parserResult );

					// Destroy HTTP response
					NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

					// Quit
					return NFALSE;
				}

				// Set name
				currentState->m_deviceName = NLib_Chaine_Dupliquer( NDEVICE_COMMON_TYPE_HUE_DEFAULT_NAME );
				break;

			case NDEVICE_TYPE_MFI_MPOWER:
				// Check response code [There are two known version, one responding 304 with a set of header, the other with 200 and {"rsp":"ok"}]
				switch( NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCode( answer ) )
				{
					default:
						break;

					case NHTTP_CODE_200_OK:
						if( ( data = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCopieData( answer ) ) != NULL )
						{
							// Check response
							if( NLib_Chaine_EstChaineContient2( data,
								NDEVICE_COMMON_TYPE_MFI_LIFECHECK_EXPECTED_ANSWER,
								NTRUE ) )
								// We don't work on uuid string
								isUUIDMustBeProcessed = NFALSE;

							// Free
							NFREE( data );
						}
						break;

					case NHTTP_CODE_302_FOUND_DOCUMENT:
						// Look for cookie header
						if( ( data = (char *)NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirAttribut( answer,
							NMOT_CLEF_REPONSE_HTTP_SET_COOKIE ) ) != NULL )
							// Check Set-cookie header value
							if( NLib_Chaine_EstChaineContient2( data,
								NDEVICE_COMMON_TYPE_MFI_COOKIE_PART,
								NTRUE ) )
								// Check location header value
								if( ( data = (char *)NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirAttribut( answer,
									NMOT_CLEF_REPONSE_HTTP_LOCATION ) ) != NULL )
									// Check location content
									if( NLib_Chaine_Comparer( data,
										NDEVICE_COMMON_TYPE_MFI_LOCATION_PART,
										NTRUE,
										0 ) )
										// We don't work on uuid string
										isUUIDMustBeProcessed = NFALSE;
						break;
				}

				// Check if work has been done
				if( isUUIDMustBeProcessed )
				{
					// Destroy
					NParser_Output_NParserOutputList_Destroy( &parserResult );

					// Destroy HTTP response
					NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

					// Quit
					return NFALSE;
				}

				// Set mFi parameters
					// Device name
						currentState->m_deviceName = NLib_Chaine_Dupliquer( NDEVICE_COMMON_TYPE_MFI_DEFAULT_NAME );
					// Random UUID
						NLib_Module_Reseau_IP_DivideIP( currentState->m_currentIP,
							NPROTOCOLE_IP_IPV4,
							(NU8*)&rawConvertedUUID );
					// Result code
						currentState->m_responseCode = NHTTP_CODE_200_OK;
				break;

			default:
				break;
		}
	}

	// Destroy HTTP response
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &answer );

	// Do we process UUID text value?
	if( isUUIDMustBeProcessed )
	{
		// Correct uuid
		if( rawUUID == NULL )
		{
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserResult );

			// Quit
			return NFALSE;
		}

		// Convert to number
		if( ( rawConvertedUUID = NDeviceCommon_UUID_ConvertUUIDToNumber( rawUUID ) ) == (NU64)0xFFFFFFFFFFFFFFFF )
		{
			// Notify
			NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

			// Destroy
			NParser_Output_NParserOutputList_Destroy( &parserResult );

			// Quit
			return NFALSE;
		}
	}

	// Export converted uuid to unified format
	if( !( currentState->m_deviceUUID = NDeviceCommon_UUID_ConvertNumberToUUID( rawConvertedUUID ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_COPY );

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &parserResult );

		// Quit
		return NFALSE;
	}

	// Save state
	currentState->m_currentState = NDEVICE_SCANNER_CURRENT_STATE_LIST_GOT_ANSWER;

	// Destroy
	NParser_Output_NParserOutputList_Destroy( &parserResult );

	// OK
	return NTRUE;
}

/**
 * Build scanner
 *
 * @param threadCount
 * 		The scanner thread count
 * @param ip
 * 		The ip to scan
 * @param cidr
 * 		The ip cidr
 * @param connectionTimeout
 * 		The connection timeout
 * @param requestTimeout
 * 		The request timeout
 * @param callbackProgress
 * 		The progress callback (a message about progress will be send everytime something happens)
 *
 * @return the scanner instance
 */
__ALLOC NDeviceScanner *NDeviceScanner_NDeviceScanner_Build( NU32 threadCount,
	const char *ip,
	NU32 cidr,
	NU32 connectionTimeout,
	NU32 requestTimeout,
	__CALLBACK void ( *callbackProgress )( const char* ) )
{
	// Output
	__OUTPUT NDeviceScanner *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NDeviceScanner ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_cidr = cidr;
	out->m_connectionTimeout = connectionTimeout;
	out->m_requestTimeout = requestTimeout;
	out->m_threadCount = threadCount;
	out->m_callbackProgress = callbackProgress;

	// Duplicate ip
	if( !( out->m_ip = NLib_Chaine_Dupliquer( ip ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Allocate thread
	if( !( out->m_thread = calloc( threadCount,
		sizeof( NThread* ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Free
		NFREE( out->m_ip );
		NFREE( out );

		// Quit
		return NULL;
	}
	// OK
	return out;
}

/**
 * Destroy the scanner
 *
 * @param this
 * 		This instance
 */
void NDeviceScanner_NDeviceScanner_Destroy( NDeviceScanner **this )
{
	// Free
	NFREE( (*this)->m_ip );
	NFREE( (*this)->m_thread );
	NFREE( (*this) );
}

/**
 * Send HTTP request (private)
 *
 * @param deviceType
 * 		The device type
 * @param clientHTTP
 * 		The http client
 * @param state
 * 		The current scanner state
 *
 * @return if request was sent successfully
 */
__PRIVATE NBOOL NDeviceScanner_NDeviceScanner_SendHTTPRequest( NDeviceType deviceType,
	NClientHTTP *clientHTTP,
	const NDeviceScannerCurrentState *state )
{
	// Request
	NRequeteHTTP *request;

	// Build request
	if( !( request = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTYPE_REQUETE_HTTP_GET,
		NDeviceCommon_Type_NDeviceType_GetElementToGET( deviceType ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add host
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
		NMOT_CLEF_REQUETE_HTTP_KEYWORD_HOST,
		state->m_currentIP );

	// Send
	return NHTTP_Client_NClientHTTP_EnvoyerRequete( clientHTTP,
		request,
		state->m_connectionTimeout );
}

/**
 * Scanning thread
 *
 * @param state
 * 		The thread state
 *
 * @return if operation succeedeed
 */
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wreturn-stack-address"
__PRIVATE __THREAD NBOOL NDeviceScanner_NDeviceScanner_ThreadScan( NDeviceScannerCurrentState *state )
{
	// Element
	NDeviceScannerResult *result;

	// IP component
	NU8 ipComponent[ 4 ];

	// Recomposed ip
	NU32 recomposedIP;

	// Iterator
	NU32 i;

	// Buffer
	char buffer[ 256 ];

	// Current ip
	char currentIP[ NLIB_TAILLE_MAXIMALE_CHAINE_IP + 1 ];

	// HTTP Client
	NClientHTTP *HTTPClient;

	// Device type
	NDeviceType deviceType;

	// Recompose ip
	recomposedIP = state->m_startIP;

	// Scan the range
	for( i = 0; i < state->m_ipCount; i++ )
	{
		// Check if running
		if( state->m_isRunning != NULL
			&& !*state->m_isRunning )
			// Quit scan
			break;

		// Increase IP count
		if( state->m_ipScanCount != NULL )
			(*state->m_ipScanCount)++;

		// Divide ip components
		NLib_Module_Reseau_IP_DivideIP2( recomposedIP,
			NPROTOCOLE_IP_IPV4,
			ipComponent );

		// Build ip
		snprintf( currentIP,
			NTAILLE_MAXIMALE_ADRESSE_IPV4 + 1,
			"%d.%d.%d.%d",
			ipComponent[ 0 ],
			ipComponent[ 1 ],
			ipComponent[ 2 ],
			ipComponent[ 3 ] );

		// Save IP address
		state->m_currentIP = currentIP;

		// Notify
		if( state->m_callbackProgress != NULL )
		{
			// Build message
			snprintf( buffer,
				256,
				"Scanner %d is scanning [%s]",
				state->m_scannerID,
				currentIP );

			// Send message
			state->m_callbackProgress( buffer );
		}

		// Try all known device types
		for( deviceType = (NDeviceType)0; deviceType < NDEVICES_TYPE; deviceType++ )
		{
			// Build client
			if( !( HTTPClient = NHTTP_Client_NClientHTTP_Construire2( currentIP,
				NDeviceCommon_Type_NDeviceType_GetListeningPort( deviceType ),
				state,
				NDeviceScanner_NDeviceScanner_CallbackReceive ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// No more scanning
				state->m_isScanning = NFALSE;

				// Quit
				return NFALSE;
			}

			// Init current state to waiting
			state->m_currentState = NDEVICE_SCANNER_CURRENT_STATE_LIST_WAITING;
			state->m_responseCode = (NHTTPCode)0;
			state->m_currentDeviceType = deviceType;

			// Send request
			if( !NDeviceScanner_NDeviceScanner_SendHTTPRequest( deviceType,
				HTTPClient,
				state ) )
			{
				// Free client
				NHTTP_Client_NClientHTTP_Detruire( &HTTPClient );

				// Continue
				continue;
			}

			// Save request time
			state->m_requestTime = NLib_Temps_ObtenirTick( );

			// Wait for answer
			while( state->m_currentState == NDEVICE_SCANNER_CURRENT_STATE_LIST_WAITING
				   && NLib_Temps_ObtenirTick( ) - state->m_requestTime < state->m_requestTimeout )
				NLib_Temps_Attendre( 16 );

			// No answer (timeout)
			if( state->m_currentState == NDEVICE_SCANNER_CURRENT_STATE_LIST_WAITING )
			{
				// Free
				NHTTP_Client_NClientHTTP_Detruire( &HTTPClient );

				// Continue
				continue;
			}

			// Check return code
			if( state->m_responseCode == NHTTP_CODE_200_OK )
			{
				// Create result
				if( !( result = NDeviceScanner_Result_NDeviceScannerResult_Build( deviceType,
					currentIP,
					state->m_deviceUUID,
					state->m_deviceName,
					state->m_requestingIP ) ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Stop scan
					state->m_isScanning = NFALSE;

					// Clean client
					NHTTP_Client_NClientHTTP_Detruire( &HTTPClient );
					NFREE( state->m_deviceUUID );
					NFREE( state->m_deviceName );
					NFREE( state->m_requestingIP );

					// Quit
					return NFALSE;
				}

				// Free
				NFREE( state->m_deviceUUID );
				NFREE( state->m_deviceName );
				NFREE( state->m_requestingIP );

				// Notify
				if( state->m_callbackProgress != NULL )
				{
					// Build message
					snprintf( buffer,
						256,
						"Found %s on [%s][%s] with scanner %d",
						NDeviceCommon_Type_NDeviceType_GetUserFriendlyName( deviceType ),
						currentIP,
						result->m_uuid,
						state->m_scannerID );

					// Send message
					state->m_callbackProgress( buffer );
				}

				// Add result
				if( !NDeviceScanner_Result_NDeviceScannerResultSet_Add( state->m_result,
					result ) )
				{
					// Notify
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Stop scan
					state->m_isScanning = NFALSE;

					// Clean client
					NHTTP_Client_NClientHTTP_Detruire( &HTTPClient );

					// Quit
					return NFALSE;
				}

				// Increase device count
				if( state->m_currentFoundDeviceCount != NULL )
					( *state->m_currentFoundDeviceCount )++;
			}

			// Free client
			NHTTP_Client_NClientHTTP_Detruire( &HTTPClient );
		}

		// Increment ip
		recomposedIP++;
	}

	// Scan is completed
	state->m_isScanning = NFALSE;

	// Free
	NFREE( state->m_deviceUUID );
	NFREE( state->m_deviceName );
	NFREE( state->m_requestingIP );

	// OK
	return NTRUE;
}
#pragma clang diagnostic pop

/**
 * Default scan
 *
 * @param this
 * 		This instance
 *
 * @return the scan result
 */
__ALLOC NDeviceScannerResultSet *NDeviceScanner_NDeviceScanner_Scan( NDeviceScanner *this )
{
	return NDeviceScanner_NDeviceScanner_Scan2( this,
		NULL,
		NULL,
		NULL );
}

/**
 * Scan
 *
 * @param this
 * 		This instance
 * @param ipScanCount
 * 		The scanned ip count
 * @param isRunning
 * 		Is running (Run until the end if set to NULL)
 * @param deviceFoundCount
 * 		The device count found until now (Can be NULL)
 *
 * @return the scan result
 */
__ALLOC NDeviceScannerResultSet *NDeviceScanner_NDeviceScanner_Scan2( NDeviceScanner *this,
	NU64 *ipScanCount,
	NBOOL *isRunning,
	NU32 *deviceFoundCount )
{
	// Output
	__OUTPUT NDeviceScannerResultSet *output;

	// State
	NDeviceScannerCurrentState *state;

	// Buffer
	char buffer[ 128 ];

	// Iterator
	NU32 i;

	// IP count
	NU32 totalIPCount;

	// IP component
	NU8 ipComponent[ 4 ];

	// The corrected IP
	NU32 correctedIP;

	// Recomposed ip
	NU32 recomposedIP;

	// IP count for this scanning thread
	NU32 ipCountThread;

	// Still analyzing?
	NBOOL isStillAnalyzing;

	// Scanner id
	NU32 scannerID = 0;

	// Effective threads count
	NU32 effectiveThreadCount;

	// Calculate how many IPs we have to analyze
	if( this->m_cidr < 32 )
		totalIPCount = NLib_Module_Reseau_CalculerNombreIPDansCIDR( this->m_cidr )
			// We don't scan network/broadcast address
			- 2;
	else
		totalIPCount = 1;

	// Divide ip
	if( !NLib_Module_Reseau_IP_DivideIP( this->m_ip,
		NPROTOCOLE_IP_IPV4,
		ipComponent ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// Correct IP considering CIDR
		// Correct IP
			correctedIP = ( ( ipComponent[ 3 ] << 24 ) & 0xFF000000 )
				| ( ( ipComponent[ 2 ] << 16 ) & 0x00FF0000 )
				| ( ( ipComponent[ 1 ] << 8 ) & 0x0000FF00 )
				| ( ipComponent[ 0 ] & 0x000000FF );
		// Apply CIDR filter
			correctedIP &= ~( totalIPCount + 1 );
		// Redivide IP
			NLib_Module_Reseau_IP_DivideIP2( correctedIP,
				NPROTOCOLE_IP_IPV4,
				ipComponent );
		// Correct order
			NLib_Memoire_Inverser( ipComponent,
				4 );

	// Create output
	if( !( output = NDeviceScanner_Result_NDeviceScannerResultSet_Build( this->m_ip,
		this->m_cidr ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Check threads amount
	if( this->m_threadCount > totalIPCount )
	{
		// Reduce threads
		effectiveThreadCount = totalIPCount;

		// Notify
		if( this->m_callbackProgress != NULL )
		{
			// Create message
			snprintf( buffer,
				128,
				"Too many scanning threads are allocated, reducing to %d...\n",
				effectiveThreadCount );

			// Notify
			this->m_callbackProgress( buffer );
		}
	}
	else
		// Keep current thread count
		effectiveThreadCount = this->m_threadCount;

	// Recompose ip
	recomposedIP = ipComponent[ 0 ]
		| ( ipComponent[ 1 ] << 8 )
		| ( ipComponent[ 2 ] << 16 )
		| ( ipComponent[ 3 ] << 24 );

	// Don't scan the network address
	recomposedIP++;

	// Create threads
	for( i = 0; i < effectiveThreadCount; i++ )
	{
		// Calculate ip count
		ipCountThread = ( i + 1 <= ( totalIPCount % effectiveThreadCount ) ) ?
			( ( totalIPCount / effectiveThreadCount ) + 1 )
			: ( totalIPCount / effectiveThreadCount );

		// Create state
		if( !( state = NDeviceScanner_NDeviceScannerCurrentState_Build( output,
			recomposedIP,
			ipCountThread,
			this->m_connectionTimeout,
			this->m_requestTimeout,
			scannerID++,
			this->m_callbackProgress,
			ipScanCount,
			isRunning,
			deviceFoundCount ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Destroy threads
			for( i--; (NS32)i >= 0; i-- )
				NLib_Thread_NThread_Detruire( &this->m_thread[ i ] );

			// Destroy output
			NDeviceScanner_Result_NDeviceScannerResultSet_Destroy( &output );

			// Quit
			return NULL;
		}

		// Create thread
		if( !( this->m_thread[ i ] = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NDeviceScanner_NDeviceScanner_ThreadScan,
			state,
			NULL ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Destroy threads
			for( i--; (NS32)i >= 0; i-- )
				NLib_Thread_NThread_Detruire( &this->m_thread[ i ] );

			// Destroy state
			NDeviceScanner_NDeviceScannerCurrentState_Destroy( &state );

			// Destroy output
			NDeviceScanner_Result_NDeviceScannerResultSet_Destroy( &output );

			// Quit
			return NULL;
		}

		// Increment ip
		recomposedIP += ipCountThread;
	}

	// Wait for threads to finish
	do
	{
		// Reset
		isStillAnalyzing = NFALSE;

		// Check for analyze
		for( i = 0; i < effectiveThreadCount; i++ )
			// Is this thread still active?
			if( ( (NDeviceScannerCurrentState*)NLib_Thread_NThread_ObtenirParametre( this->m_thread[ i ] ) )->m_isScanning )
			{
				// Yes it is
				isStillAnalyzing = NTRUE;

				// We're done for the activeness lookup
				break;
			}

		// Wait
		if( isStillAnalyzing )
			NLib_Temps_Attendre( 16 );
	} while( isStillAnalyzing );

	// Clean threads
	for( i = 0; i < effectiveThreadCount; i++ )
	{
		// Get parameter
		state = NLib_Thread_NThread_ObtenirParametre( this->m_thread[ i ] );

		// Detroy thread
		NLib_Thread_NThread_Detruire( &this->m_thread[ i ] );

		// Clean parameter
		NDeviceScanner_NDeviceScannerCurrentState_Destroy( &state );
	}

	// OK
	return output;
}

