#include "../include/NDeviceScanner.h"

// -------------------------------------------------
// struct NDeviceScanner::NDeviceScannerCurrentState
// -------------------------------------------------

/**
 * Build state
 *
 * @param result
 * 		The result list
 * @param startIp
 * 		The starting ip
 * @param ipCount
 * 		The ip to scan count
 * @param connectionTimeout
 * 		The connection timeout
 * @param requestTimeout
 * 		The request timeout
 * @param scannerID
 * 		The scanner id
 * @param callbackProgress
 * 		The progress callback
 * @param ipScanCount
 * 		The scanned ip count (can be NULL)
 * @param isRunning
 * 		Is running ? (Running until the end if set NULL)
 * @param currentFoundDeviceCount
 * 		The current found device count (Can be NULL)
 *
 * @return the instance
 */
__ALLOC NDeviceScannerCurrentState *NDeviceScanner_NDeviceScannerCurrentState_Build( NDeviceScannerResultSet *result,
	NU32 startIp,
	NU32 ipCount,
	NU32 connectionTimeout,
	NU32 requestTimeout,
	NU32 scannerID,
	__CALLBACK void ( ___cdecl *callbackProgress )( const char* ),
	NU64 *ipScanCount,
	NBOOL *isRunning,
	NU32 *currentFoundDeviceCount )
{
	// Output
	__OUTPUT NDeviceScannerCurrentState *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NDeviceScannerCurrentState ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save
	out->m_result = result;
	out->m_startIP = startIp;
	out->m_ipCount = ipCount;
	out->m_connectionTimeout = connectionTimeout;
	out->m_requestTimeout = requestTimeout;
	out->m_scannerID = scannerID;
	out->m_callbackProgress = callbackProgress;
	out->m_ipScanCount = ipScanCount;
	out->m_isRunning = isRunning;
	out->m_currentFoundDeviceCount = currentFoundDeviceCount;

	// Init
	out->m_currentState = NDEVICE_SCANNER_CURRENT_STATE_LIST_WAITING;
	out->m_isScanning = NTRUE;

	// OK
	return out;
}

/**
 * Destroy state
 *
 * @param this
 * 		This instance
 */
void NDeviceScanner_NDeviceScannerCurrentState_Destroy( NDeviceScannerCurrentState **this )
{
	// Free
	NFREE( (*this) );
}

