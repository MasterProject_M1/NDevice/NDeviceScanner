NDevice Scanner
===============

Introduction
------------

This project is about scanning devices connected on a network with a specified
network address/cidr.

This tool is for IPv4 address scanning.

It will iterate through all IPs in the given range, and for each IP, check if
the given element (HUE=/debug/clip.html, GHOST=/ghost/info, ...) responds with
a 200 OK code

Extension
---------

Last version allows user to choose the scanning threads count

Dependencies
------------

This project depends on projects:

- NLib
- NHTTP
- NDeviceCommon
- NParser
- NJson

Example
-------

Activate test entry point with NDEVICE_SCANNER_TEST_PROJECT

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/Device/NDeviceScanner

