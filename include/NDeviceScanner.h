#ifndef NDEVICESCANNER_PROTECT
#define NDEVICESCANNER_PROTECT

// ------------------------
// namespace NDeviceScanner
// ------------------------

// namespace NLib
#include "../../../NLib/NLib/NLib/include/NLib/NLib.h"

// namespace NHTTP
#include "../../../NLib/NHTTP/include/NHTTP.h"

// namespace NParser
#include "../../../NParser/NParser/include/NParser.h"

// namespace NJson
#include "../../../NParser/NJson/include/NJson.h"

// namespace NDeviceCommon
#include "../../NDeviceCommon/include/NDeviceCommon.h"

// namespace NDeviceScanner::Result
#include "Result/NDeviceScanner_Result.h"

// enum NDeviceScanner::NDeviceScannerCurrentStateList
#include "NDeviceScanner_NDeviceScannerCurrentStateList.h"

// struct NDeviceScanner::NDeviceScannerCurrentState
#include "NDeviceScanner_NDeviceScannerCurrentState.h"

// struct NDeviceScanner::NDeviceScanner
#include "NDeviceScanner_NDeviceScanner.h"

#endif // !NDEVICESCANNER_PROTECT

