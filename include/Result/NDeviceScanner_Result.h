#ifndef NDEVICESCANNER_RESULT_PROTECT
#define NDEVICESCANNER_RESULT_PROTECT

// --------------------------------
// namespace NDeviceScanner::Result
// --------------------------------

// struct NDeviceScanner::Result::NDeviceScannerResult
#include "NDeviceScanner_Result_NDeviceScannerResult.h"

// struct NDeviceScanner::Result::NDeviceScannerResultSet
#include "NDeviceScanner_Result_NDeviceScannerResultSet.h"

#endif // !NDEVICESCANNER_RESULT_PROTECT
