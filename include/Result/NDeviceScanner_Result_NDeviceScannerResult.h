#ifndef NDEVICESCANNER_RESULT_NDEVICESCANNERRESULT_PROTECT
#define NDEVICESCANNER_RESULT_NDEVICESCANNERRESULT_PROTECT

// ---------------------------------------------------
// struct NDeviceScanner::Result::NDeviceScannerResult
// ---------------------------------------------------

typedef struct NDeviceScannerResult
{
	// Device type
	NDeviceType m_type;

	// IP
	char *m_ip;

	// UUID
	char *m_uuid;

	// Name
	char *m_name;

	// Requesting IP
	char *m_requestingIP;
} NDeviceScannerResult;

/**
 * Build a device
 *
 * @param type
 * 		The device type
 * @param ip
 * 		The ip
 * @param uuid
 * 		The device uuid
 * @param name
 * 		The device name
 * @param requestingIP
 * 		The requesting IP
 *
 * @return the device scanner result instance
 */
__ALLOC NDeviceScannerResult *NDeviceScanner_Result_NDeviceScannerResult_Build( NDeviceType type,
	const char *ip,
	const char *uuid,
	const char *name,
	const char *requestingIP );

/**
 * Destroy a device
 *
 * @param this
 * 		This instance
 */
void NDeviceScanner_Result_NDeviceScannerResult_Destroy( NDeviceScannerResult** );

/**
 * Get ip
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NDeviceScanner_Result_NDeviceScannerResult_GetIP( const NDeviceScannerResult* );

/**
 * Get type
 *
 * @param this
 * 		This instance
 *
 * @return the type
 */
NDeviceType NDeviceScanner_Result_NDeviceScannerResult_GetType( const NDeviceScannerResult* );

/**
 * Get UUID
 *
 * @param this
 * 		This instance
 *
 * @return the uuid
 */
const char *NDeviceScanner_Result_NDeviceScannerResult_GetUUID( const NDeviceScannerResult* );

/**
 * Get name
 *
 * @param this
 * 		This instance
 *
 * @return the name
 */
const char *NDeviceScanner_Result_NDeviceScannerResult_GetName( const NDeviceScannerResult* );

/**
 * Get requesting ip
 *
 * @param this
 * 		This instance
 *
 * @return the requesting ip
 */
const char *NDeviceScanner_Result_NDeviceScannerResult_GetRequestingIP( const NDeviceScannerResult* );

#endif // !NDEVICESCANNER_RESULT_NDEVICESCANNERRESULT_PROTECT
