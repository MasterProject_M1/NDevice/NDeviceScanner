#ifndef NDEVICESCANNER_RESULT_NDEVICESCANNERRESULTSET_PROTECT
#define NDEVICESCANNER_RESULT_NDEVICESCANNERRESULTSET_PROTECT

// ------------------------------------------------------
// struct NDeviceScanner::Result::NDeviceScannerResultSet
// ------------------------------------------------------

typedef struct NDeviceScannerResultSet
{
	// Result list
	NListe *m_list;

	// IP
	char *m_ip;

	// CIDR
	NU32 m_cidr;
} NDeviceScannerResultSet;

/**
 * Build a scan result set
 *
 * @param ip
 * 		The IPv4
 * @param cidr
 * 		The cidr
 *
 * @return the scan result
 */
__ALLOC NDeviceScannerResultSet *NDeviceScanner_Result_NDeviceScannerResultSet_Build( const char *ip,
	NU32 cidr );

/**
 * Destroy scan result set
 *
 * @param this
 * 		This instance
 */
void NDeviceScanner_Result_NDeviceScannerResultSet_Destroy( NDeviceScannerResultSet** );

/**
 * Get result count
 *
 * @param this
 * 		This instance
 *
 * @return the result count
 */
NU32 NDeviceScanner_Result_NDeviceScannerResultSet_GetCount( const NDeviceScannerResultSet* );

/**
 * Get result through index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The index
 *
 * @return the result
 */
const NDeviceScannerResult *NDeviceScanner_Result_NDeviceScannerResultSet_Get( const NDeviceScannerResultSet*,
	NU32 index );

/**
 * Add result
 *
 * @param this
 * 		This instance
 * @param result
 * 		The result to add
 *
 * @return if operation succeedeed
 */
NBOOL NDeviceScanner_Result_NDeviceScannerResultSet_Add( NDeviceScannerResultSet*,
	__WILLBEOWNED NDeviceScannerResult *result );

#endif // !NDEVICESCANNER_RESULT_NDEVICESCANNERRESULTSET_PROTECT
