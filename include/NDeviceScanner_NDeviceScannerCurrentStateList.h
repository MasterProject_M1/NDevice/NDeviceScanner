#ifndef NDEVICESCANNER_NDEVICESCANNERCURRENTSTATELIST_PROTECT
#define NDEVICESCANNER_NDEVICESCANNERCURRENTSTATELIST_PROTECT

// ---------------------------------------------------
// enum NDeviceScanner::NDeviceScannerCurrentStateList
// ---------------------------------------------------

typedef enum NDeviceScannerCurrentStateList
{
	NDEVICE_SCANNER_CURRENT_STATE_LIST_WAITING,
	NDEVICE_SCANNER_CURRENT_STATE_LIST_GOT_ANSWER,

	NDEVICE_SCANNER_CURRENT_STATES_LIST
} NDeviceScannerCurrentStateList;

#endif // !NDEVICESCANNER_NDEVICESCANNERCURRENTSTATELIST_PROTECT
