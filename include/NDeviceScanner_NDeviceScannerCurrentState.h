#ifndef NDEVICESCANNER_NDEVICESCANNERCURRENTSTATE_PROTECT
#define NDEVICESCANNER_NDEVICESCANNERCURRENTSTATE_PROTECT

// -------------------------------------------------
// struct NDeviceScanner::NDeviceScannerCurrentState
// -------------------------------------------------

typedef struct NDeviceScannerCurrentState
{
	// Is running?
	NBOOL *m_isRunning;

	// Result set
	NDeviceScannerResultSet *m_result;

	// Current state
	NDeviceScannerCurrentStateList m_currentState;

	// Response code
	NHTTPCode m_responseCode;

	// Request time
	NU32 m_requestTime;

	// Start ip
	NU32 m_startIP;

	// IP count to scan
	NU32 m_ipCount;

	// The connection timeout
	NU32 m_connectionTimeout;

	// The request timeout
	NU32 m_requestTimeout;

	// Is scanning?
	NBOOL m_isScanning;

	// ID
	NU32 m_scannerID;

	// IP scan count
	NU64 *m_ipScanCount;

	// Current IP
	const char *m_currentIP;

	// Current found device count
	NU32 *m_currentFoundDeviceCount;

	// Current device type
	NDeviceType m_currentDeviceType;

	// Device uuid
	char *m_deviceUUID;

	// Requesting IP
	char *m_requestingIP;

	// Device name
	char *m_deviceName;

	// Progress callback
	void ( ___cdecl *m_callbackProgress )( const char* );
} NDeviceScannerCurrentState;

/**
 * Build state
 *
 * @param result
 * 		The result list
 * @param startIp
 * 		The starting ip
 * @param ipCount
 * 		The ip to scan count
 * @param connectionTimeout
 * 		The connection timeout
 * @param requestTimeout
 * 		The request timeout
 * @param scannerID
 * 		The scanner id
 * @param callbackProgress
 * 		The progress callback
 * @param ipScanCount
 * 		The scanned ip count (can be NULL)
 * @param isRunning
 * 		Is running ? (Running until the end if set NULL)
 * @param currentFoundDeviceCount
 * 		The current found device count (Can be NULL)
 *
 * @return the instance
 */
__ALLOC NDeviceScannerCurrentState *NDeviceScanner_NDeviceScannerCurrentState_Build( NDeviceScannerResultSet *result,
	NU32 startIp,
	NU32 ipCount,
	NU32 connectionTimeout,
	NU32 requestTimeout,
	NU32 scannerID,
	__CALLBACK void ( ___cdecl *callbackProgress )( const char* ),
	NU64 *ipScanCount,
	NBOOL *isRunning,
	NU32 *currentFoundDeviceCount );

/**
 * Destroy state
 *
 * @param this
 * 		This instance
 */
void NDeviceScanner_NDeviceScannerCurrentState_Destroy( NDeviceScannerCurrentState** );

#endif // !NDEVICESCANNER_NDEVICESCANNERCURRENTSTATE_PROTECT
