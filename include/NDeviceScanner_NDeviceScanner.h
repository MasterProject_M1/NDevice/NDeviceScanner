#ifndef NDEVICESCANNER_NDEVICESCANNER_PROTECT
#define NDEVICESCANNER_NDEVICESCANNER_PROTECT

// -------------------------------------
// struct NDeviceScanner::NDeviceScanner
// -------------------------------------

typedef struct NDeviceScanner
{
	// Threads
	NThread **m_thread;

	// Thread count
	NU32 m_threadCount;

	// IP to scan
	char *m_ip;

	// IP's CIDR
	NU32 m_cidr;

	// The connection timeout
	NU32 m_connectionTimeout;

	// The request timeout
	NU32 m_requestTimeout;

	// The progress callback
	__CALLBACK void ( ___cdecl *m_callbackProgress )( const char * );
} NDeviceScanner;

/**
 * Build scanner
 *
 * @param threadCount
 * 		The scanner thread count
 * @param ip
 * 		The ip to scan
 * @param cidr
 * 		The ip cidr
 * @param connectionTimeout
 * 		The connection timeout
 * @param requestTimeout
 * 		The request timeout
 * @param callbackProgress
 * 		The progress callback, can be NULL (a message about progress will be send everytime something happens)
 *
 * @return the scanner instance
 */
__ALLOC NDeviceScanner *NDeviceScanner_NDeviceScanner_Build( NU32 threadCount,
	const char *ip,
	NU32 cidr,
	NU32 connectionTimeout,
	NU32 requestTimeout,
	__CALLBACK void ( *callbackProgress )( const char* ) );

/**
 * Destroy the scanner
 *
 * @param this
 * 		This instance
 */
void NDeviceScanner_NDeviceScanner_Destroy( NDeviceScanner** );

/**
 * Default scan
 *
 * @param this
 * 		This instance
 *
 * @return the scan result
 */
__ALLOC NDeviceScannerResultSet *NDeviceScanner_NDeviceScanner_Scan( NDeviceScanner* );

/**
 * Scan
 *
 * @param this
 * 		This instance
 * @param ipScanCount
 * 		The scanned ip count
 * @param isRunning
 * 		Is running (Run until the end if set to NULL)
 * @param deviceFoundCount
 * 		The device count found until now (Can be NULL)
 *
 * @return the scan result
 */
__ALLOC NDeviceScannerResultSet *NDeviceScanner_NDeviceScanner_Scan2( NDeviceScanner *this,
	NU64 *ipScanCount,
	NBOOL *isRunning,
	NU32 *deviceFoundCount );

#endif // !NDEVICESCANNER_NDEVICESCANNER_PROTECT

